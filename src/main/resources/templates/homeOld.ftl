<#import "blocks/common.ftl" as common />
<!--FreeMarkerTemplate-->

<!DOCTYPE html>
<html lang="en">
<@common.head title="Home" />
<body>
    <#list topics as topic>
        topic.id
        topic.title
        topic.author
    </#list>
</body>
</html>