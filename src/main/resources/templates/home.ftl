<#import "blocks/common.ftl" as common />

<!DOCTYPE html>
<html lang="en">
<@common.head title="Forum Name" />
<body>
    <@common.header title="Forum Name" email=login />
    <main>
        <div class="topic-list">
            <#list topics as topic> 
            <div class="topic">
                <h3 class="topic-name"><a href="/topics/${topic.id}">${topic.header}</a></h3>
                <div class="topic-author">${topic.author.name}</div>
            </div>
            </#list>
        </div>
    </main>
</body>
</html>