<#import "blocks/common.ftl" as common />

<!DOCTYPE html>
<html lang="en">
    <@common.head title="Forum Name" />
    <body>
        <@common.header title="Forum Name" email=login />
        <main>
            <div class="post-list">
                <h2 class="topic-title">${topic.header}</h2>
                <#list topic.messages as message>

                <div class="post">
                    <div class="post-author">
                        <div class="user-name">${message.author.name}</div>
                    </div>
                    <div class="post-contents">
                        <div class="post-date">${(message.createDateTime).format('dd.MM.yyyy HH:mm:ss')}</div>
                        <div class="post-message">${message.text}</div>
                    </div>
                </div>
                </#list>
            </div>
        </main>
    </body>
</html>