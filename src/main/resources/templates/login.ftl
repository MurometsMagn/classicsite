<#import "blocks/common.ftl" as common />

<!DOCTYPE html>
<html lang="en">
<@common.head title="Login" />
<body>
    <h1>Please login</h1>
    <form method="POST" action="/auth/login">
        <div>
            <label for="login">login:</label>
            <input type="text" id="login" name="login">
        </div>
        <div>
            <label for="password">password:</label>
            <input type="password" id="password" name="password">
        </div>
        <div>
            <input type="submit" value="login">
        </div>
    </form>
</body>
</html>
