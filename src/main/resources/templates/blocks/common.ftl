<#macro head title="Forum">
    <head>
        <title>${title}</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/style.css">
        <script src="/js/jquery-3.6.0.min.js"></script>
        <script src="/js/main.js"></script>
    </head>
</#macro>

<#macro header title email>
    <header>
        <h1 class="site-name">${title}</h1>
        <div class="filler"></div>
        <div class="login">${email}</div>
    </header>
</#macro>

