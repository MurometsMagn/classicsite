package com.example.ClassicSite.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class TopicWithMessagesDTO {
    private String header;
    private List<MessageGetDTO> messages;
}
