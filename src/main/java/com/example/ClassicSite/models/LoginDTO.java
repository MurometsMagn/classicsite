package com.example.ClassicSite.models;

import com.google.gson.Gson;
import lombok.Getter;
import lombok.Setter;

//в отличие от UserPostDTO должен содержать ТОЛЬКО логин и пароль
@Getter
@Setter
public class LoginDTO {
    private String login;
    private String password;

    public LoginDTO(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String toJSON() {
        return new Gson().toJson(this);
    }
}

//Todo: 9.06.22
/*
 отобразить имейл пользователя, который залогинен
 */
