package com.example.ClassicSite.models;

import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

@Getter
@Setter
public class TopicGetDTO  extends TopicBaseDTO{
    private long id;
    private AuthorGetDTO author;
    private ZonedDateTime createDateTime;

    public TopicGetDTO(long id, AuthorGetDTO author, String header, ZonedDateTime createDateTime) {
        super(header);
        this.id = id;
        this.author = author;
        this.createDateTime = createDateTime;
    }
}
