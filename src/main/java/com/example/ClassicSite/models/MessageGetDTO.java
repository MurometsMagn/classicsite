package com.example.ClassicSite.models;

import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

@Getter
@Setter
public class MessageGetDTO extends MessageBaseDTO {
    private long id;
    private AuthorGetDTO author;
    private ZonedDateTime createDateTime;

    public MessageGetDTO(long id, AuthorGetDTO author, long topicId, String text, ZonedDateTime createDateTime) {
        super(text, topicId);
        this.id = id;
        this.author = author;
        this.createDateTime = createDateTime;
    }
}