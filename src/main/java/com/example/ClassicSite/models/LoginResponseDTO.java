package com.example.ClassicSite.models;

import com.google.gson.Gson;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginResponseDTO {
    private String token;

    public LoginResponseDTO(String token) {
        this.token = token;
    }

    public static LoginResponseDTO fromJSON(String json) {
        return new Gson().fromJson(json, LoginResponseDTO.class);
    }
}
