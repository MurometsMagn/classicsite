package com.example.ClassicSite.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TopicItem {
    private int id;
    private String name;
    private String topicAuthor;
}
