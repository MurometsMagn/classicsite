package com.example.ClassicSite.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageBaseDTO {
    private String text;
    private long topicId;

    public MessageBaseDTO(String text, long topicId) {
        this.text = text;
        this.topicId = topicId;
    }
}
