package com.example.ClassicSite.controllers;

import com.example.ClassicSite.exceptions.UnauthorizedException;
import com.example.ClassicSite.models.LoginDTO;
import com.example.ClassicSite.services.BackendService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

@Controller
@RequestMapping("/auth")
public class AuthController {
    private final BackendService backendService;

    public AuthController(BackendService backendService) {
        this.backendService = backendService;
    }

    @GetMapping("/login")
    public String login() { //задача - отрендерить шаблон для страницы входа
        return "login";
    }

    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String auth(@ModelAttribute LoginDTO loginDTO, HttpSession session) {
        String login = loginDTO.getLogin();
        String password = loginDTO.getPassword();
        try {
            String token = backendService.login(login, password);
            session.setAttribute("token", token);
            session.setAttribute("login", login);
            return "redirect:/";
        } catch (UnauthorizedException | IOException | InterruptedException e) {
            return "login";
        }
    }


}

//todo: 18.01.22, 20.01.22. 25.01.22
/*
  1. в AuthController вызвать BackEndService для login, полученный токен записать в сессию
  2. реализовать шаблон login.ftl
    2.1. создать модельный класс логинДТО с полями логин и пассворд //уже был
    2.2. принять его как параметр метода auth в AuthController
  3. реализовать интерсептор реализующий проверку авторизации
 */
