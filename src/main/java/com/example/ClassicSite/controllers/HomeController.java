package com.example.ClassicSite.controllers;

import com.example.ClassicSite.models.TopicGetDTO;
import com.example.ClassicSite.models.TopicWithMessagesDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;

@Slf4j
@Controller
public class HomeController {
    private final ObjectMapper objectMapper;

    public HomeController(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @GetMapping("/google")
    public String showGooglePage() {
        return "redirect:https://google.com";
//        return "home";
    }

//    @GetMapping  //  ("/") обработку корня можно не писать
//    public ModelAndView showHomePage(HttpSession session) {
//        String token = (String) session.getAttribute("token");
//        log.info(token);
//        ModelAndView modelAndView = new ModelAndView("home");
//        modelAndView.addObject("person", new Person("Vasya"));
//        modelAndView.addObject("person1", new Person("Petya"));
//
//        return modelAndView;
//    }

    @GetMapping("/showViewPage")
    public String passParametersWithModel(Model model) {
        Map<String, String> map = new HashMap<>();
        map.put("spring", "mvc");
        model.addAttribute("message", "Baeldung");
        model.mergeAttributes(map);
        return "viewPage";
    }

    @GetMapping("/")
    public String home(Model model, HttpSession session) throws URISyntaxException, IOException, InterruptedException {
        String login = session.getAttribute("login").toString();
        String token = session.getAttribute("token").toString();
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(new URI("http://localhost:8888/topics"))
                .GET()
                .header("Authorization", "Bearer " + token)
                .build();
        HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        log.debug(httpResponse.statusCode() + "");
        if (httpResponse.statusCode() == 200) {
            String body = httpResponse.body();
            List<TopicGetDTO> topics = Arrays.asList(objectMapper.readValue(body, TopicGetDTO[].class));
            log.debug(topics.size() + "");
            model.addAttribute("topics", topics);
        } else {
            model.addAttribute("topics", List.of());
        }
        model.addAttribute("login", login);
        return "home";
    }

    @GetMapping("/topics/{id}")
    public String topics(@PathVariable int id, Model model, HttpSession session) throws URISyntaxException, IOException, InterruptedException {
        String login = session.getAttribute("login").toString();
        String token = session.getAttribute("token").toString();
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(new URI("http://localhost:8888/topics/" + id))
                .GET()
                .header("Authorization", "Bearer " + token)
                .build();
        HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        if (httpResponse.statusCode() == 200) {
            String body = httpResponse.body();
            TopicWithMessagesDTO data = objectMapper.readValue(body, TopicWithMessagesDTO.class);
            model.addAttribute("topic", data);
        } else {
            model.addAttribute("topic", null);
        }
        model.addAttribute("login", login);
        return "topic";
    }
}

/*
  EndPoint - (конечная точка) - это метод контроллера, обрабатывающий определенный адрес и метод протокола http,
  например, GET /users.  Эта пара также может называться endpoint.
  T.е. это как пара метод-адрес, так и обработчик этих запросов.
 */


//todo: hometask 17.03.22
/*
  1. получить от бекэнда список всех топиков
  2. преобразовать этот список в список topicItem-ов
  3. передать этот список в ftl
 */

//todo: hometask 31.03.22
/*
  1.реализовать /topics/id на бэкэнде
  2.реализовать /topics/id на фронтэнде
  3.встроить данные от сервера в topic.ftl
 */