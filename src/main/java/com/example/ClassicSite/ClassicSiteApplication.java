package com.example.ClassicSite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassicSiteApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClassicSiteApplication.class, args);
	}

}
