package com.example.ClassicSite.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.http.HttpResponse;

@Slf4j
public class AuthInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        /*
        return true - если нужно передать управление следующему интерсептеру или контроллеру,т.е. норм поведение по умолч.
        return false - мы обработали запрос и сформировали ответ, дальше идти не нужно, т.е. обработана ошибка.
         */
        String path = request.getRequestURI();
        if (path.equals("/error")) return true;

        HttpSession session = request.getSession();
        Object token = session.getAttribute("token");
        log.debug(path);
        if (token == null && !path.equals("/auth/login")) {
            response.sendRedirect("/auth/login");
            return false;
        }

        return true;
    }
}

//todo: homeTask 01.02.22
/*
 1. реализовать макрос topicsListItem который отображает один топик в общем списке топиков
 2. изменить home.ftl чтобы отображать список топиков на форуме
 */
