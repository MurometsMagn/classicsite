package com.example.ClassicSite.exceptions;

public class UnauthorizedException extends Exception {

    public UnauthorizedException() {
        super("unauthorized");
    }
}
