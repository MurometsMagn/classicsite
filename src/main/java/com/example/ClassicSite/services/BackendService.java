package com.example.ClassicSite.services;

import com.example.ClassicSite.exceptions.UnauthorizedException;
import com.example.ClassicSite.models.LoginDTO;
import com.example.ClassicSite.models.LoginResponseDTO;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
public class BackendService {
    private static final String BACKEND_URL = "http://localhost:8888";
    private final HttpClient httpClient = HttpClient.newHttpClient();

    public String login(String login, String password) //returns jwt
            throws UnauthorizedException, IOException, InterruptedException {
        LoginDTO loginDTO = new LoginDTO(login, password);
        String request = loginDTO.toJSON();
        try {
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .uri(new URI(BACKEND_URL + "/users/login"))
                    .POST(HttpRequest.BodyPublishers.ofString(request))
                    .header("Content-Type", "application/json")
                    .build();
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != 200) {
                throw new UnauthorizedException();
            }
            LoginResponseDTO responseDTO = LoginResponseDTO.fromJSON(response.body());
            return responseDTO.getToken();
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return "";
        }
    }
}

//throw как и return прерывает ф-ю