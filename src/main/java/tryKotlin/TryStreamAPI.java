package tryKotlin;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TryStreamAPI {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(1, 2, 3);

        //напечатать все нечетные эл-ты массива
        for (var number : numbers) {
            if (number % 2 != 0) {
                System.out.println(number);
            }
        }

        numbers.stream()
                .filter(number -> number%2 != 0) //оставляет только нужные значения
                .forEach(System.out::println); //.forEach(number -> System.out.println(number));
        //стрим - это фор, а фильтр - это иф внутри фора, а форИч - это действие, которое мы хотим выполнить внутри фора

        //на основе массива numbers создать массив doubles, который содержит все эл-ты numbers умноженные на 2
        List<Integer> doubles = new ArrayList<>();

        for (var number : numbers) {
            doubles.add(number * 2);
        }

        doubles = numbers.stream()
                .map(number -> number * 2) //преобразует кол-ю в соотв-ии с заданными правилами
                .collect(Collectors.toList());

        //посчитать сумму эл-тов кол-ии
        int accumulator = 0;
        for (var d : doubles) {
            accumulator += d;
        }

        doubles.stream()
                .reduce((acc, d) -> acc + d)
                .ifPresent(System.out::println);
    }
}

/*
 cтримы в джава позволяют выполнять последовательные операции над кол-ями.
 Каждая такая операция принимает на вход коллекцию-результат предыдущей операции, обрабатывает ее (к-л сп-бом)
 и возвращает новую псевдоколлекцию (т.е. еще не материализованную). Таким образом стрим предст-ет собой
 конвейер, обрабатывающий исходную кол-ю. В конце этого конвейера обычно находится терминальная операция, которая
 не продолжает стрим. Примеры таких операций: это collect, forEach.


  numbers.forEach(System.out::println);
 */
