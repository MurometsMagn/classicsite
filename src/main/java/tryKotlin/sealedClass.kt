package tryKotlin

fun main(args: Array<String>) {
    val db = Db.MongoDb(5, "mongo")
    val db2 = Db.PostgreSQL(5, "postgres", true)

    val dbCopy = db.copy(conn = "Done")
    if (db == dbCopy) println("Они равны")
    else println("Они не равны")

    if (db is Db.MongoDb) db.printInfo()

    val list = listOf("Java", "PHP", "Perl", "JavaScript", "C++")
    //filterList(list, {it.startsWith("P")}) //в фигурных скобках передается ф-я
    filterList(list) {
        it.startsWith("P")
    }
    //filterList(list, filter)
}

sealed class Db { //изолированный класс
    data class MySQL(val id: Int, val conn: String) : Db()
    data class MongoDb(val id: Int, val conn: String) : Db() {
        fun printInfo() {
            println("MongoDb has id: $id and connection: $conn")
        }
    }

    data class PostgreSQL(val id: Int, val conn: String, val isDone: Boolean) : Db()
    object Help : Db() {
        val conn = "Connection done"
    }
}

//поле вне класса
val Db.PostgreSQL.info: String
    get() = "PostgreSQL has id: $id and connection: $conn"

//функция вне класса
fun Db.PostgreSQL.printInfo() {
    println(info)
}

//ф-я принимает ф-ю
fun filterList(list: List<String>, filter: (String) -> Boolean) {
    list.forEach { el ->
        if (filter(el)) println(el)
    }
}

val filter: (String) -> Boolean = {
    it.startsWith("J")
}

