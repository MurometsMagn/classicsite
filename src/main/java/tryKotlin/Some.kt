package tryKotlin

class Some {
    companion object {
        var count = 0
    }

    init {
        count++
        println("Создано объектов: $count")
    }
}

fun main(args: Array<String>) {
    val test = Some()
    val test2 = Some()
    val test3 = Some()
    val test4 = Some()
}