package tryKotlin;

import lombok.Data;

//dsl domain specific language
public sealed class JavaSealedClass permits InheritedClass, SecondInheritedClass {

}

final class SecondInheritedClass extends JavaSealedClass {}

